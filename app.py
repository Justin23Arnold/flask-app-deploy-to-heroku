import os
from pprint import pprint
import requests
from flask import Flask, jsonify, render_template, request

server = Flask(__name__, static_url_path='/static')
private_token = "pk.addf779a6ccc6dc99c20e9c74f39da2f"
url = "https://us1.locationiq.com/v1/search.php"
weather_key = "48305fd79571a3833a854b44b00b8a58"

MIN_LEN = 5

def address_validator(address):
    errors = {}
    if len(address) < 5:
        errors['length'] = 'Address needs to be longer than 5 characters'

    else:
        split_address = address.split(',')
        print(split_address)
        if split_address[0][0].isalpha():
            errors['format_error'] = 'Address must start with a number'
    
    return errors

@server.route("/", methods=['Get', 'Post'])
def api_weather():
    """
    welcome page
    """
    error = None
    
    if request.method == 'POST':
        errors = address_validator(request.form['address'])

        if len(errors) == 0:
            address = request.form['address']
            data = {
            'key': private_token,
            'q': address,
            'format': 'json'
            }

            response = requests.get(url, params=data)

            latitude = response.json()[0]['lat']
            longitude = response.json()[0]['lon']

            try:
                weather_response = requests.get(f'https://api.openweathermap.org/data/2.5/onecall?lat={latitude}&lon={longitude}&units=imperial&appid={weather_key}')
            except Exception as e:
                raise e

            weather = weather_response.json()
            current_weather = weather['current']['weather'][0]['description']
            humidity = weather['current']['humidity']
            temp = round(weather['current']['temp'])
            feels_like_temp = round(weather['current']['feels_like'])
            wind_speed = round(weather['current']['wind_speed'])
            if wind_speed > 0:
                wind_gust = round(weather['current']['wind_gust'])
            else:
                wind_gust = wind_speed
            info = { 
                'curr_weather' : current_weather,
                'humidity' : humidity,
                'curr_temp' : temp,
                'feels_like_temp' : feels_like_temp,
                'wind_speed' : wind_speed, 
                'wind_gust' : wind_gust,
            }
            
            return render_template('geo.html', address = address, **info)
        else:
            errors_to_show = errors.values()
            return render_template('index.html', errors = errors_to_show)

    return render_template('index.html')

if __name__ == '__main__':
    server.run(host='0.0.0.0', debug=True)